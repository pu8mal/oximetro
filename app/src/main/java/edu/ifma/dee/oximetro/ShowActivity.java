package edu.ifma.dee.oximetro;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class ShowActivity extends AppCompatActivity {

    ConnectionThread connectionThread;
    static TextView textBpm;
    static TextView textSpo2;
    static TextView textStatus;

    static Double bpm;
    static Integer spo2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        textBpm =  (TextView) findViewById(R.id.textBPM);
        textSpo2 =  (TextView) findViewById(R.id.textSPO2);
        textStatus = (TextView) findViewById(R.id.textStatus);

        connectionThread = new ConnectionThread("20:17:02:16:33:36");
        connectionThread.start();
        try{
            connectionThread.sleep(1000);
        }catch (Exception E){
            E.printStackTrace();
        }
    }

    public static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg){
            Bundle bundle = msg.getData();
            byte[] data = bundle.getByteArray("data");
            String dataString = new String(data);
            JSONObject oximeter;

            if(dataString.equals("---N")){
                textStatus.setText("Desconectado");
            }else if (dataString.equals("---S")){
                textStatus.setText("Conectado");
            }else {
                try {
                    oximeter = new JSONObject(dataString);
                    bpm = oximeter.getDouble("bpm");
                    spo2 = oximeter.getInt("spo2");
                    textBpm.setText(bpm.toString());
                    textSpo2.setText(spo2.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                textStatus.setText(dataString);
            }
        }
    };
}
