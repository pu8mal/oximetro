package edu.ifma.dee.oximetro;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 1;
    BluetoothAdapter bluetoothAdapter;
    List<String> devices;
    List<String> address;
    ArrayAdapter<String> adapters;
    Set<BluetoothDevice> pairedDevices;

    ListView deviceList;
    Intent showActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        showActivity = new Intent(this, ShowActivity.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deviceList = (ListView) findViewById(R.id.device_list);


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter != null){
            if(!bluetoothAdapter.isEnabled()){
                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBT , REQUEST_ENABLE_BT);
            }
        } else {
            //sem suporte
        }

        devices = new ArrayList<String>();
        address = new ArrayList<String>();
        adapters = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, devices);
        refreshAdapter(null);
        deviceList.setAdapter(adapters);
        deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                showActivity.putExtra("addr", address.get(position));
                startActivity(showActivity);
            }
        });

    }

    public void refreshAdapter(View v){
        adapters.clear();
        pairedDevices = bluetoothAdapter.getBondedDevices();
        if(pairedDevices.size() > 0)
            for(BluetoothDevice device : pairedDevices) {
                adapters.add(device.getName());
                address.add(device.getAddress());
            }
    }
}