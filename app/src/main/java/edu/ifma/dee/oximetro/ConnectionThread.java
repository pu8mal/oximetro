package edu.ifma.dee.oximetro;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.UUID;

public class ConnectionThread extends Thread {
    BluetoothSocket btSocket = null;
    BluetoothServerSocket btServerSocket = null;
    String btDevAddress;
    String myUUID = "00001101-0000-1000-8000-00805F9B34FB";
    InputStream input;
    OutputStream output;
    boolean server;
    boolean running;
    boolean isConnected;

    public ConnectionThread(){
        this.server = true;
    }

    public ConnectionThread(String btDevAddress){
        this.server = false;
        this.btDevAddress = btDevAddress;
    }

    public void run(){
        this.running = true;
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(this.server){
            try{
                btServerSocket = btAdapter.listenUsingInsecureRfcommWithServiceRecord("Oximetro", UUID.fromString(myUUID));
                btSocket = btServerSocket.accept();

                if(btSocket != null);
            }
            catch (IOException e)
            {
                e.printStackTrace();
                toShowActivity("---N".getBytes());
            }
        }else{
            try{
                BluetoothDevice btDevice = btAdapter.getRemoteDevice(btDevAddress);
                btSocket = btDevice.createRfcommSocketToServiceRecord(UUID.fromString(myUUID));

                btAdapter.cancelDiscovery();

                if(btSocket != null){
                    btSocket.connect();
                }
            }catch (IOException e){
                e.printStackTrace();
                toShowActivity("---N".getBytes());
            }
        }

        if(btSocket != null){
            this.isConnected = true;
            toShowActivity("---S".getBytes());
            try{
                input = btSocket.getInputStream();
                output = btSocket.getOutputStream();
                while (running){

                    byte[] buffer = new byte[1024];
                    int bytes;
                    int bytesRead = -1;

                    do{
                        bytes = input.read(buffer, bytesRead+1,1);
                        bytesRead += bytes;
                    }while(buffer[bytesRead] != '\n');

                    toShowActivity(Arrays.copyOfRange(buffer, 0, bytesRead-1));
                }
            }catch(IOException e){
                e.printStackTrace();
                toShowActivity("---N".getBytes());
                this.isConnected = false;
            }
        }
    }

    private void toShowActivity(byte[] data){
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putByteArray("data", data);
        message.setData(bundle);
        ShowActivity.handler.sendMessage(message);
    }

    public void cancel() {

        try {
            running = false;
            this.isConnected = false;
            btServerSocket.close();
            btSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        running = false;
        this.isConnected = false;
    }

    public boolean isConnected() {
        return this.isConnected;
    }
}
